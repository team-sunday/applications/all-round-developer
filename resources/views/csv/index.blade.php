@extends('layouts.app')

@section('content')
    <table class="table table-striped table-bordered">
        <thead>
        <tr class="text-center">
            <td>ID</td>
            <td>Technology</td>
            <td>Description</td>
            <td>Experience</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $key => $value)
            <tr class="text-center">
                <td>{{ $value->id }}</td>
                <td>{{ $value->technology }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->experience }}</td>

                <td class="d-flex justify-content-around">

                    <!-- delete the shark (uses the destroy method DESTROY /sharks/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->

                    <!-- show the shark (uses the show method found at GET /sharks/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('csv/' . $value->id) }}">Show Record</a>

                    <!-- edit this shark (uses the edit method found at GET /sharks/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('csv/' . $value->id . '/edit') }}">Edit Record</a>

                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
@endsection
