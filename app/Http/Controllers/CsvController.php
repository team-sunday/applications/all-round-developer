<?php

namespace App\Http\Controllers;

use App\Csv;
use App\Exports\CsvExport;
use App\Imports\CsvImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class CsvController extends Controller
{
    public function export()
    {
        return Excel::download(new CsvExport, 'data.csv');
    }

    public function import()
    {
        Excel::import(new CsvImport, request()->file('file'));

        return back();
    }


    public function index()
    {
        return view('csv/index', [
            'data' => Csv::all()
        ]);
    }

    public function show($id)
    {
        return view('csv/show', [
            'data' => Csv::find($id)
        ]);
    }

    public function edit($id)
    {
        return view('csv/edit', [
            'data' => Csv::find($id)
        ]);
    }

    public function update(Request $request, Csv $csv)
    {
        $request->validate([
            'technology' => 'required',
            'description' => 'required',
            'experience' => 'required',
        ]);

        $csv->update($request->all());
        $csv->save();

        return redirect()->route('csv.index')
            ->with('success','Product updated successfully');
    }

}
